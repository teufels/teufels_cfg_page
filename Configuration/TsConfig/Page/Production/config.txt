##################################
## CONFIGURATION FOR COPY PASTE ##
##################################
TCEMAIN.table.pages {
  disablePrependAtCopy = 1
  disableHideAtCopy = 0
}

TCEMAIN.table.tt_content {
  disablePrependAtCopy = 1
  disableHideAtCopy = 0
}

#####################################
## CONFIGURATION FOR IMAGE EFFECTS ##
#####################################
## See also ext_tables.php
## See also TypoScript/setup.txt in tx_teufels_thm_blazy or tx_teufels_thm_jq_echo
TCEFORM.tt_content.image_effects.addItems {
    30 = Bootstrap: .img-rounded
    31 = Bootstrap: .img-circle
    32 = Bootstrap: .img-thumbnail
}